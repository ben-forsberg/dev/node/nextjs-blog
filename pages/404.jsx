import utilStyles from "../styles/utils.module.css";

export default function Custom404() {
  return (
    <div>
      <h1
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
        className={utilStyles.headingLg}
      >
        404 - Page Not Found
      </h1>
      <p
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
        className={utilStyles.headingLg}
      >
        You should probably let Ben know.
      </p>
    </div>
  );
}
