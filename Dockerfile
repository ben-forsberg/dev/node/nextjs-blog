FROM node:20 as builder
WORKDIR /app
COPY . .

RUN NODE_ENV=production npm run build:ci


FROM node:slim as runtime
WORKDIR /app

COPY --from=builder /app/.next /app/.next
COPY --from=builder /app/node_modules /app/node_modules
COPY public /app/public
COPY package*.json /app

ENV NEXT_TELEMETRY_DISABLED=1
EXPOSE 8080
CMD ["npm", "run", "start"]

